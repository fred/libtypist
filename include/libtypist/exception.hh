/*
 * Copyright (C) 2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef EXCEPTION_HH_EE2DD6A3E39747D1B9036AD826649A18
#define EXCEPTION_HH_EE2DD6A3E39747D1B9036AD826649A18

#include <exception>
#include <string>

namespace LibTypist {

class Exception : public std::exception
{
public:
    explicit Exception(std::string _msg);
    const char* what() const noexcept override;

private:
    std::string msg_;
};

class HttpResponseResult : public Exception
{
public:
    HttpResponseResult(
            int _result,
            std::string _result_text,
            std::string _reason);
    int result() const;
    const std::string& result_text() const;
    const std::string& reason() const;

private:
    int result_;
    std::string result_text_;
    std::string reason_;
};

} // namespace LibTypist

#endif
