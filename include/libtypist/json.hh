/*
 * Copyright (C) 2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef JSON_HH_FC015C2D21C24709AB08D36CB7B60E9A
#define JSON_HH_FC015C2D21C24709AB08D36CB7B60E9A

#include "libtypist/struct.hh"

#include <string>

namespace LibTypist {

std::string to_json_string(const Struct& _struct);


} // namespace LibTypist

#endif
