/*
 * Copyright (C) 2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef CONNECTION_HH_CD686B31C76C4F0281287EFD0BA2D26E
#define CONNECTION_HH_CD686B31C76C4F0281287EFD0BA2D26E

#include "src/conf.hh"

#include "libtypist/strong_type.hh"

namespace LibTypist {

class Connection
{
public:
    using ConnectionString = StrongString<struct ConnectionStringTag_>;
    using Host = StrongString<struct HostTag_>;
    using Port = StrongString<struct PortTag_>;
    using Authorization = StrongString<struct AuthorizationTag_>;

#ifdef LIBTYPIST_USE_SSL
    enum struct SslPolicy
    {
        never,
        always,
        autodetect
    };

    explicit Connection(const ConnectionString& _connection_string, SslPolicy = SslPolicy::autodetect);
#else
    explicit Connection(const ConnectionString& _connection_string);
#endif

    const Host& get_host() const;
    const Port& get_port() const;
    const Authorization& get_authorization() const;
    Connection& set_authorization(Authorization _authorization);
#ifdef LIBTYPIST_USE_SSL
    SslPolicy get_ssl_policy() const;
#endif

private:
    Host host_;
    Port port_;
    Authorization authorization_;
#ifdef LIBTYPIST_USE_SSL
    SslPolicy ssl_policy_;
#endif
};

} // namespace LibTypist

#endif
