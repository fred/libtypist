/*
 * Copyright (C) 2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef STRUCT_HH_F8D5B4B5647B438B8F2091FDFD12B78E
#define STRUCT_HH_F8D5B4B5647B438B8F2091FDFD12B78E

#include "libtypist/strong_type.hh"

#include <boost/none_t.hpp>
#include <boost/variant.hpp>

#include <map>
#include <string>
#include <vector>

namespace LibTypist {

using StructKey = StrongString<struct StructKeyTag_>;
struct StructValue;
using Struct = std::map<const StructKey, StructValue>;
struct StructValue
{
    // TODO explicit constructors to solve "const char* -> std::string" ?
    using Value = boost::variant<Struct, std::vector<StructValue>, bool, int, std::string, boost::none_t>;
    Value value;
};

} // namespace LibTypist

#endif
