/*
 * Copyright (C) 2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef RENDER_HH_396E5D87DBAA409C8234F2E1F4817C5E
#define RENDER_HH_396E5D87DBAA409C8234F2E1F4817C5E

#include "libstrong/type.hh"
#include "libtypist/connection.hh"
#include "libtypist/exception.hh"
#include "libtypist/strong_type.hh"
#include "libtypist/struct.hh"

#include <iostream>

namespace LibTypist {

using Target = StrongString<struct TargetTag_>;
using Accept = StrongString<struct AcceptTag_>;

void render_plain(
        const Connection::Host& _host,
        const Connection::Port& _port,
        const Connection::Authorization& _authorization,
        const Target& _target,
        const Accept& _accept,
        std::istream& _data_json,
        std::ostream& _output);

#ifdef LIBTYPIST_USE_SSL
void render_ssl(
        const Connection::Host& _host,
        const Connection::Port& _port,
        const Connection::Authorization& _authorization,
        const Target& _target,
        const Accept& _accept,
        std::istream& _data_json,
        std::ostream& _output);
#endif

using TemplateName = StrongString<struct TemplateNameTag_>;

void render(
        const Connection& _connection,
        const TemplateName& _template_name,
        const Accept& _accept,
        const Struct& _context,
        std::ostream& _output);

} // namespace LibTypist

#endif
