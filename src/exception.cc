/*
 * Copyright (C) 2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "include/libtypist/exception.hh"

namespace LibTypist {

Exception::Exception(std::string _msg)
    : msg_{std::move(_msg)}
{
}

const char* Exception::what() const noexcept
{
    return msg_.c_str();
}

HttpResponseResult::HttpResponseResult(
        int _result,
        std::string _result_text,
        std::string _reason)
    : Exception("HTTP Response Result Code: " + std::to_string(_result) + " - " + _result_text + " (" + _reason + ")"),
      result_(_result),
      result_text_(std::move(_result_text)),
      reason_(std::move(_reason))
{
}

int HttpResponseResult::result() const
{
    return result_;
}

const std::string& HttpResponseResult::result_text() const
{
    return result_text_;
}

const std::string& HttpResponseResult::reason() const
{
    return reason_;
}

} // namespace LibTypist
