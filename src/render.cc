/*
 * Copyright (C) 2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "include/libtypist/render.hh"

#include "include/libtypist/exception.hh"

#include "include/libtypist/json.hh"

#include <boost/asio/connect.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/beast/core.hpp>
#include <boost/beast/http.hpp>
#include <boost/beast/version.hpp>

#include "src/conf.hh"

#ifdef LIBTYPIST_USE_SSL
#include <boost/beast/ssl.hpp>
#include <boost/asio/ssl/error.hpp>
#include <boost/asio/ssl/stream.hpp>
#endif

#include <cstdlib>
#include <iostream>
#include <string>

namespace LibTypist {

namespace {

namespace http = boost::beast::http;

http::request<http::string_body> make_http_request(
        const Connection::Host& _host,
        const Connection::Authorization& _authorization,
        const Target& _target,
        const Accept& _accept,
        std::istream& _data_json)
{
    const auto http_version{11};
    http::request<http::string_body> http_request{http::verb::post, *_target, http_version};
    http_request.set(http::field::host, *_host);
    http_request.set(http::field::user_agent, BOOST_BEAST_VERSION_STRING);
    http_request.set(http::field::content_type, "application/json");
    http_request.set(http::field::accept, *_accept);
    if (!_authorization->empty())
    {
        http_request.set(http::field::authorization, "Token " + *_authorization);
    }
    http_request.body() = std::string(std::istreambuf_iterator<char>(_data_json), {});
    http_request.set(http::field::content_length, std::to_string(http_request.body().size()));
    return http_request;
} // namespace LibTypist::{anonymous}

http::response<http::string_body> rpc(
        boost::beast::tcp_stream& _tcp_stream,
        const http::request<http::string_body>& _http_request)
{
    http::response<http::string_body> http_response;
    http::write(_tcp_stream, _http_request);
    boost::beast::flat_buffer buffer;
    http::read(_tcp_stream, buffer, http_response);
    return http_response;
}

#ifdef LIBTYPIST_USE_SSL
http::response<http::string_body> rpc(
        boost::beast::ssl_stream<boost::beast::tcp_stream>& _ssl_stream,
        const http::request<http::string_body>& _http_request)
{
    http::response<http::string_body> http_response;
    http::write(_ssl_stream, _http_request);
    boost::beast::flat_buffer buffer;
    http::read(_ssl_stream, buffer, http_response);
    return http_response;
}
#endif

} // namespace LibTypist::{anonymous}

void render_plain(
        const Connection::Host& _host,
        const Connection::Port& _port,
        const Connection::Authorization& _authorization,
        const Target& _target,
        const Accept& _accept,
        std::istream& _data_json,
        std::ostream& _output)
{
    namespace http = boost::beast::http;

    boost::asio::io_context io_context;

    boost::asio::ip::tcp::resolver tcp_resolver(io_context);
    auto const resolver_results = tcp_resolver.resolve(*_host, *_port);

    boost::beast::tcp_stream tcp_stream(io_context);
    tcp_stream.connect(resolver_results);

    const auto http_request = make_http_request(_host, _authorization, _target, _accept, _data_json);
    const auto http_response = rpc(tcp_stream, http_request);

    if (http_response.result() != http::status::ok)
    {
        std::ostringstream result;
        result << http_response.result();
        std::ostringstream reason;
        reason << http_response.reason();
        throw HttpResponseResult(http_response.result_int(), result.str(), reason.str());
    }

    _output << http_response.body();

    boost::beast::error_code err_code;
    tcp_stream.socket().shutdown(boost::asio::ip::tcp::socket::shutdown_both, err_code);
    if (err_code && err_code != boost::beast::errc::not_connected)
    {
        //throw boost::beast::system_error{err_code};
        throw Exception{"libtypist: error when closing stream: " + err_code.message()};
    }
}

#ifdef LIBTYPIST_USE_SSL
void render_ssl(
        const Connection::Host& _host,
        const Connection::Port& _port,
        const Connection::Authorization& _authorization,
        const Target& _target,
        const Accept& _accept,
        std::istream& _data_json,
        std::ostream& _output)
{
    namespace http = boost::beast::http;

    boost::asio::io_context io_context;

    boost::asio::ssl::context ctx(boost::asio::ssl::context::tlsv12_client);
    // TODO cert verification

    boost::asio::ip::tcp::resolver tcp_resolver(io_context);
    auto const resolver_results = tcp_resolver.resolve(*_host, *_port);

    boost::beast::ssl_stream<boost::beast::tcp_stream> ssl_stream(io_context, ctx);
    boost::beast::get_lowest_layer(ssl_stream).connect(resolver_results);
    ssl_stream.handshake(boost::asio::ssl::stream_base::client);

    const auto http_request = make_http_request(_host, _authorization, _target, _accept, _data_json);
    const auto http_response = rpc(ssl_stream, http_request);

    if (http_response.result() != http::status::ok)
    {
        std::ostringstream result;
        result << http_response.result();
        std::ostringstream reason;
        reason << http_response.reason();
        throw HttpResponseResult(http_response.result_int(), result.str(), reason.str());
    }

    _output << http_response.body();

    boost::beast::error_code err_code;
    ssl_stream.shutdown(err_code);
    if (err_code == boost::asio::error::eof)
    {
        err_code = {};
    }
    if (err_code == boost::asio::ssl::error::stream_truncated)
    {
        // ignore unexpected shutdown without propper SSL close notification
        err_code = {};
    }
    if (err_code)
    {
        //throw boost::beast::system_error{err_code};
        throw Exception{"libtypist: error when closing stream: " + err_code.message()};
    }
}
#endif

void render(
        const Connection& _connection,
        const TemplateName& _template_name,
        const Accept& _accept,
        const Struct& _context,
        std::ostream& _output)
{
    Target target{"/api/templates/active/" + *_template_name + "/render/"};
    std::istringstream stream_json{to_json_string(_context)};
#ifdef LIBTYPIST_USE_SSL
    const auto use_ssl = _connection.get_ssl_policy() == Connection::SslPolicy::always ||
                        (_connection.get_ssl_policy() == Connection::SslPolicy::autodetect &&
                                (*(_connection.get_port()) == "443" || *(_connection.get_port()) == "https"));
    if (use_ssl)
    {
        render_ssl(_connection.get_host(), _connection.get_port(), _connection.get_authorization(), target, _accept, stream_json, _output);
    }
    else
#endif
    {
        render_plain(_connection.get_host(), _connection.get_port(), _connection.get_authorization(), target, _accept, stream_json, _output);
    }
}

} // namespace LibTypist
