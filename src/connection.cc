/*
 * Copyright (C) 2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "include/libtypist/connection.hh"

#include "include/libtypist/exception.hh"

#include <algorithm>
#include <string>
#include <utility>
#include <vector>

namespace LibTypist {

namespace {

std::tuple<Connection::Host, Connection::Port> split_connection_string(const Connection::ConnectionString& _connection_string)
{
    if (_connection_string->empty())
    {
        throw Exception("connection string empty");
    }

    if (_connection_string->at(0) == '[')
    {
        const auto pos = _connection_string->find(']');
        if (pos == std::string::npos)
        {
            throw Exception("connection string invalid, found opening bracket '[' but not closing bracket ']'");
        }
        if (pos + 2 > _connection_string->length())
        {
            throw Exception("connection string invalid");
        }
        return {Connection::Host{_connection_string->substr(1, pos - 1)}, Connection::Port{_connection_string->substr(pos + 2)}};
    }
    else if (std::count(_connection_string->begin(), _connection_string->end(), ':') > 1)
    {
        return {Connection::Host{*_connection_string}, Connection::Port{""}};
    }
    else
    {
        const auto pos = _connection_string->find(':');
        if (pos != std::string::npos && pos == _connection_string->length() - 1)
        {
            throw Exception("connection string invalid");
        }
        return {Connection::Host{_connection_string->substr(0, pos)},
                Connection::Port{pos != std::string::npos ? _connection_string->substr(pos + 1) : ""}};
    }
}

} // namespace LibTypist::{anonymous}

Connection::Connection(const ConnectionString& _connection_string
#ifdef LIBTYPIST_USE_SSL
        , SslPolicy _ssl_policy
#endif
)
{
    const auto host_and_port = split_connection_string(_connection_string);
    host_ = std::get<Connection::Host>(host_and_port);
    port_ = std::get<Connection::Port>(host_and_port);
#ifdef LIBTYPIST_USE_SSL
    ssl_policy_ = _ssl_policy;
#endif
}

const Connection::Host& Connection::get_host() const
{
    return host_;
}

const Connection::Port& Connection::get_port() const
{
    return port_;
}

const Connection::Authorization& Connection::get_authorization() const
{
    return authorization_;
}

Connection& Connection::set_authorization(Authorization _authorization)
{
    authorization_ = std::move(_authorization);
    return *this;
}

#ifdef LIBTYPIST_USE_SSL
Connection::SslPolicy Connection::get_ssl_policy() const
{
    return ssl_policy_;
}
#endif

} // namespace LibTypist
