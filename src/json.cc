/*
 * Copyright (C) 2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "include/libtypist/json.hh"

#include <nlohmann/json.hpp>

namespace LibTypist {

using json = nlohmann::json;

namespace {

void json_wrap(bool _src, json& _json)
{
    _json = _src;
}

void json_wrap(int _src, json& _json)
{
    _json = _src;
}

void json_wrap(std::string _src, json& _json)
{
    _json = std::move(_src);
}

void json_wrap(boost::none_t, json& _json)
{
    _json = nullptr;
}

void json_wrap(const Struct& _src, json& _json);

void json_wrap(const std::vector<StructValue>& _src, json& _json)
{
    for (const auto& item : _src)
    {
        json json_data;
        boost::apply_visitor(
                [&](auto i)
                {
                    json_wrap(std::forward<decltype(i)>(i), json_data);
                },
                item.value);
        _json.push_back(json_data);
    }
}

void json_wrap(const Struct& _src, json& _json)
{
    for (const auto& item : _src)
    {
        json json_data;
        boost::apply_visitor(
                [&](auto i)
                {
                    json_wrap(std::forward<decltype(i)>(i), json_data);
                },
                item.second.value);
        _json[*item.first] = json_data;
    }
}

} // namespace LibTypist::{anonymous}

std::string to_json_string(const Struct& _struct)
{
    json json_data;

    json_wrap(_struct, json_data);

    return json_data.dump();
}

} // namespace LibTypist
