ChangeLog
=========

.. contents:: Releases
   :backlinks: none
   :local:

1.0.0 (2022-05-27)
------------------

* Initial ``LibTypist`` implementation
