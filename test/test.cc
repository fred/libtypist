/*
 * Copyright (C) 2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "libtypist/render.hh"

#include <fstream>
#include <iostream>
#include <sstream>

int main(int argc, char* argv[])
{
    if (argc != 5)
    {
        std::cerr <<
            "Usage: " << argv[0] << " <host> <port> <target> <json_request>\n" <<
            "Example:\n"
            "    " << argv[0] << " localhost 80 /render.pdf \"{...}\"" << std::endl;
        return EXIT_FAILURE;
    }

    std::istringstream data_json(argv[4]);

    {
        std::ofstream output_pdf("test-result.pdf", std::ofstream::out);
        try
        {
            LibTypist::render_plain(
                    LibTypist::Connection::Host{argv[1]},
                    LibTypist::Connection::Port{argv[2]},
                    LibTypist::Connection::Authorization{std::string{""}},
                    LibTypist::Target{argv[3]},
                    LibTypist::Accept{"application/pdf"},
                    data_json,
                    output_pdf);
            output_pdf.close();
        }
        catch (const std::exception& e)
        {
            std::cerr << "std::exception caught while creating file: " << e.what() << std::endl;
            output_pdf.close();
        }
        catch (...)
        {
            std::cerr << "exception caught while creating file" << std::endl;
            output_pdf.close();
        }
    }

    //{
    //    std::ofstream output_pdf("test-result2.pdf", std::ofstream::out);
    //    try
    //    {
    //        LibTypist::render(
    //                LibTypist::Connection(LibTypist::Connection::ConnectionString{std::string{argv[1]} + ":" + argv[2]}),
    //                LibTypist::TemplateName{argv[3]},
    //                LibTypist::ContentType{"text/html"},
    //                LibTypist::Struct{
    //                    {LibTypist::StructKey{"key1"},
    //                     LibTypist::StructValue{std::string{"value1"}}},
    //                    {LibTypist::StructKey{"key2"},
    //                     LibTypist::StructValue{2}},
    //                    {LibTypist::StructKey{"key3bool"},
    //                     LibTypist::StructValue{true}},
    //                    {LibTypist::StructKey{"key4bool"},
    //                     LibTypist::StructValue{false}},
    //                    {LibTypist::StructKey{"key5arr"},
    //                         LibTypist::StructValue{std::vector<LibTypist::StructValue>{
    //                             {std::string{"arrval1"}},
    //                             {42},
    //                             {std::string{"arrval2"}},
    //                             {true},
    //                             {false},
    //                             LibTypist::StructValue{std::vector<LibTypist::StructValue>{
    //                                 {0}, {7}, {boost::none}
    //                             }}
    //                         }}
    //                    }
    //                },
    //                output_pdf);
    //        output_pdf.close();
    //    }
    //    catch (const std::exception& e)
    //    {
    //        std::cerr << "std::exception caught while creating file: " << e.what() << std::endl;
    //        output_pdf.close();
    //    }
    //    catch (...)
    //    {
    //        std::cerr << "exception caught while creating file" << std::endl;
    //        output_pdf.close();
    //    }
    //}
    return EXIT_SUCCESS;
}
